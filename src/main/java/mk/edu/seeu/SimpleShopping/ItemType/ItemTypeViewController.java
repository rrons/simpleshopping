package mk.edu.seeu.SimpleShopping.ItemType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/types")
public class ItemTypeViewController {
    private final ItemTypeRepository itemTypeRepository;

    public ItemTypeViewController(ItemTypeRepository itemTypeRepository) {
        this.itemTypeRepository = itemTypeRepository;
    }

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute("itemTypes", itemTypeRepository.findAll());
        return "itemTypes";
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable(name = "id") Long id, Model model) {
        model.addAttribute("itemType", itemTypeRepository.findById(id).orElseThrow(EntityNotFoundException::new));
        return "itemType";
    }

    @PostMapping
    public String save(@ModelAttribute ItemType itemType) {
        System.out.println(itemType.getId());
        itemTypeRepository.save(itemType);
        return "redirect:/items";
    }

    @GetMapping("/create")
    public String create(Model model) {
        ItemType itemType = new ItemType();
        model.addAttribute("itemType", itemType);
        model.addAttribute("action", "/types");
        return "itemType_create";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable Long id, Model model) {
        ItemType itemType = itemTypeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        model.addAttribute("itemType", itemType);
        model.addAttribute("action", "/types/" + id + "/update");
        return "itemType_create";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute ItemType itemType, @PathVariable long id) {
        ItemType type = itemTypeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        type.setName(itemType.getName());
        itemTypeRepository.save(type);
        return "redirect:/types";
    }
}
