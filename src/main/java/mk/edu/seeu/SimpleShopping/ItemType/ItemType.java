package mk.edu.seeu.SimpleShopping.ItemType;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import mk.edu.seeu.SimpleShopping.CartItem.CartItem;
import mk.edu.seeu.SimpleShopping.ItemCategory.ItemCategory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ItemType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToMany
    private List<CartItem> cartItems = new ArrayList<>();
    @ManyToMany
    @JsonIdentityReference
    private List<ItemCategory> categories = new ArrayList<>();

    public ItemType() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public List<ItemCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<ItemCategory> categories) {
        this.categories = categories;
    }
}
