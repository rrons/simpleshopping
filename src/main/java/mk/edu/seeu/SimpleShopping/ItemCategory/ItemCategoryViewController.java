package mk.edu.seeu.SimpleShopping.ItemCategory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/categories")
public class ItemCategoryViewController {
    
    private final ItemCategoryRepository itemCategoryRepository;

    public ItemCategoryViewController(ItemCategoryRepository itemCategoryRepository) {
        this.itemCategoryRepository = itemCategoryRepository;
    }

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute("itemCategories", itemCategoryRepository.findAll());
        return "itemCategories";
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable(name = "id") Long id, Model model) {
        model.addAttribute("itemCategory", itemCategoryRepository.findById(id).orElseThrow(EntityNotFoundException::new));
        return "itemCategory";
    }

    @PostMapping
    public String save(@ModelAttribute ItemCategory itemCategory) {
        System.out.println(itemCategory.getId());
        itemCategoryRepository.save(itemCategory);
        return "redirect:/categories";
    }

    @GetMapping("/create")
    public String create(Model model) {
        ItemCategory itemCategory = new ItemCategory();
        model.addAttribute("itemCategory", itemCategory);
        model.addAttribute("action", "/categories");
        return "itemCategory_create";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable Long id, Model model) {
        ItemCategory itemCategory = itemCategoryRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        model.addAttribute("itemCategory", itemCategory);
        model.addAttribute("action", "/categories/" + id + "/update");
        return "itemCategory_create";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute ItemCategory itemCategory, @PathVariable long id) {
        ItemCategory category = itemCategoryRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        category.setName(itemCategory.getName());
        itemCategoryRepository.save(category);
        return "redirect:/categories";
    }
}
