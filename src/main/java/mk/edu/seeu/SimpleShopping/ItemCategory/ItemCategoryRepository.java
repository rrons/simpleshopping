package mk.edu.seeu.SimpleShopping.ItemCategory;

import mk.edu.seeu.SimpleShopping.ItemType.ItemType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemCategoryRepository extends JpaRepository<ItemCategory, Long>, PagingAndSortingRepository<ItemCategory, Long> {
    List<ItemCategory> findAllByItemTypesContains(ItemType itemType);
}
