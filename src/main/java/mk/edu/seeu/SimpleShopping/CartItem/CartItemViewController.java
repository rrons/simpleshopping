package mk.edu.seeu.SimpleShopping.CartItem;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/items")
public class CartItemViewController {
    private final CartItemRepository cartItemRepository;

    public CartItemViewController(CartItemRepository cartItemRepository) {
        this.cartItemRepository = cartItemRepository;
    }

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute("cartItems", cartItemRepository.findAll());
        return "cartItems";
    }

    @GetMapping("/page/{page_number}")
    public String findAllPaginated(@PathVariable("page_number") int pageNumber, Model model) {
        model.addAttribute("cartItems", cartItemRepository.findAll(PageRequest.of(pageNumber, 2)));
        return "cartItems";
    }

    @GetMapping("/sorted")
    public String findAllPaginated(Model model) {
        model.addAttribute("cartItems", cartItemRepository.findAll(Sort.by(Sort.Direction.ASC, "amount")));
        return "cartItems";
    }

    @GetMapping("/ordered")
    public String findAllOrdered(Model model) {
        model.addAttribute("cartItems", cartItemRepository.findAllByOrderByAmount());
        return "cartItems";
    }

    @GetMapping("/by_type/{name}")
    public String findAllTypeName(@PathVariable String name, Model model) {
        model.addAttribute("cartItems",
                cartItemRepository.findAllByItemTypeName(name));
        return "cartItems";
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable(name = "id") Long id, Model model) {
        model.addAttribute("cartItem", cartItemRepository.findById(id).orElseThrow(EntityNotFoundException::new));
        return "cartItem";
    }

    @PostMapping
    public String save(@ModelAttribute CartItem cartItem) {
        System.out.println(cartItem.getId());
        cartItemRepository.save(cartItem);
        return "redirect:/items";
    }

    @GetMapping("/create")
    public String create(Model model) {
        CartItem cartItem = new CartItem();
        model.addAttribute("cartItem", cartItem);
        model.addAttribute("action", "/items");
        return "cartItem_create";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable Long id, Model model) {
        CartItem cartItem = cartItemRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        model.addAttribute("cartItem", cartItem);
        model.addAttribute("action", "/items/" + id + "/update");
        return "cartItem_create";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute CartItem cartItem, @PathVariable long id) {
        CartItem item = cartItemRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        item.setAmount(cartItem.getAmount());
        cartItemRepository.save(item);
        return "redirect:/items";
    }
}
