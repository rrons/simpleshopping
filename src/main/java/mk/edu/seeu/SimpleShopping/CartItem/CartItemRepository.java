package mk.edu.seeu.SimpleShopping.CartItem;

import mk.edu.seeu.SimpleShopping.ItemType.ItemType;
import mk.edu.seeu.SimpleShopping.ShoppingCart.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long>, PagingAndSortingRepository<CartItem, Long> {
    List<CartItem> findAll();
    List<CartItem> findAllByItemType(ItemType itemType);
    List<CartItem> findAllByItemTypeName(String name);
    List<CartItem> findAllByShoppingCart(ShoppingCart shoppingCart);
    List<CartItem> findAllByOrderByAmount();

}
